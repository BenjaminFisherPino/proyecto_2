Proyecto n°2 “proyecto.py”
 
 
En este archivo README.md se detallara el funcionamiento del algoritmo desarrollado por nuestro grupo de trabajo. Este texto se estructurara de la siguiente forma, primero se detallaran las 
instrucciones para poder descargar y correr este programa, luego una breve introducción a lo que es el PEP-8 y de que forma lo plasmamos en nuestro código, después se hablara de las variables 
utilizadas, posteriormente se detallará el funcionamiento de las 4 funciones principales y por ultimo se explicara como se unen y interactúan unas con otras para hacer que el código funcione.
 
Para descargar este programa lo primero es tener un computador con sistema operativo Linux que tenga la versión 3 de python, posteriormente se debe instalar el programa “GIT” directamente desde 
la terminal de su ordenador Linux con el comando “sudo apt install git”. Luego de esto, uno debe proceder a crear su cuenta de GitLab, en caso de ya tener una omitir este paso. Lo siguiente es 
dirigirse al repositorio < https://gitlab.com/BenjaminFisherPino/proyecto_2.git > y presionar la opción “Clone” seleccionando el formato https para copiarlo. Después uno debe dirigirse a la 
terminal y escribir el comando “git clone < https://gitlab.com/BenjaminFisherPino/proyecto_2.git >, para finalizar escribir el comando “cd proyecto_2” y seguido “python3 proyecto.py”. Tras haber 
seguido estos pasos deberías poder interactuar con el programa sin mayor problema. Tras iniciado el programa se imprimirá un menú en donde aparecerán 4 incógnitas a resolver, las cuales se 
escogen mediante números, una vez ejecutada y analizada por el usuario, se le dará al usuario la opción de escoger otra pregunta o finalizar el programa.
 
El PEP-8 es un documento publicado el 5 de julio del año 2001 por Guido Van Rossum, la finalidad de este texto es establecer una pauta a la hora de escribir código en lenguaje Python. Cabe 
destacar que esta pauta no es necesaria para que un algoritmo funcione, pero es recomendable seguirla para optimizar el proceso de análisis y comprensión de estos. Para la confección de nuestro 
algoritmo se utilizo PEP-8 para hacer nuestro código más comprensible y legible al ojo de un programador, se utilizo el “identado”(anglicismo que proviene de la palabra identation que a su vez 
se traduce como sangría) de 4 espacios, las líneas de más de 79 caracteres de largo se cortan en líneas más cortas y se alinea el primer carácter de la primera línea con el primer carácter de la 
segunda línea y así sucesivamente. Además, si se utilizan operadores (and, or, =, +, -, etc) en aquellas lineas, se corta de tal forma que el operador nunca quede como primer carácter en la 
siguiente línea (la linea ya cortada), no se mezclaron “TAB” con espacios para evitar confusiones con el identado y por último se utilizo el lenguaje español para escribir los comentarios 
considerando que este código sólo va a ser leído por hispanohablantes. En el algoritmo usamos una cantidad de 7 variables, cada una almacena partes del archivo, como por ejemplo la variable 
“data” que almacena el archivo en sí, esta se ejecuta en la función de “abre_archivo” para luego retornar a la variable como resultado, y ser usada en las demás variables.

Se crea una función menú, en donde se le señala cuales son las las opciones que tiene el usuario para realizar en el programa, el programa tiene 4 funciones principales, cada una de ellas 
trabaja, una de las preguntas, a continuación, se explicara cual es el ejercicio que realiza cada función. La primera función “pregunta_1, está asociada a la relación que tiene el consumo de 
carnes y los casos de covid activos. Arroja como resultado, el mayor y menor índice de consumo de carnes de los paises y se ocupa un iloc en una de las variables para seleccionar línea y columnas 
en específico. La segunda función “pregunta_2” arroja el resultado de países con mayor porcentaje de población contagiada, para esto se usa el mismo método de la primera función. La función 
“pregunta_3” entrega el resultado en relación con la obesidad y los casos mortales de covid, junto con los países más sanos con relación a la cantidad de casos recuperados, la función busca el 
mayor y menor índice de obesidad de los países. La función “pregunta_4” asocia directamente el consumo de alcohol y el porcentaje de obesidad, para ello el algoritmo busca el mayor índice de los 
países respecto a estos datos. La función “selecciona_pregunta”, esta funciona te muestra el menú, y te da la opción de escoger una de las preguntas, enumeradas del 1 al 4, al ingresar una opción 
incorrecta el programa lanza un mensaje diciendo "opción invalida, por favor asegúrese de que su respuesta sea 1, 2, 3 o 4", para esta función, se usó un While el cual repite la pregunta hasta 
que el usuario entregue un valor reconocido por el programa, el usuario puede realizar esta acción las veces que desee, ya que hay otro While, que cumple con la función de finalizar el programa, 
es decir, se le hace una pregunta al usuario, la cual es ¿Desea continuar?, para esta respuesta, se usa el While como se menciona anteriormente, al escribir “si” este programa finalizará de forma 
inmediata. También esta la función main, que es la mas importante, esta función, llama a las demás funciones para que se ejecuten y se logre correr el programa

Esperamos como grupo que este archivo README.md le haya sido útil para entender los requerimientos y el funcionamiento del programa, en caso de que le surja una duda que no haya sido respondida 
en este texto o que no haya sido abarcada totalmente no dude en contactarse con algún miembro del equipo. Gracias por leer este texto.
 
Autores:
 -Benjamín Fisher bfisher20@alumnos.utalca.cl
 -Cristóbal Briceño cbriceno20@alumnos.utalca.cl
 -Felipe Carreño fcarreno20@alumnos.utalca.cl

