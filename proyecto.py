#!/usr/bin/env python3
# _*_ coding: utf-8 _*_

import os
import pandas as pd

FILENAME = "./suministro_alimentos_kcal.csv"

def abre_archivo():
    data = pd.read_csv(FILENAME)
    return data

def clear():
    os.system("clear")

def menu():
    print("Presione:\n\n"
          "  [1] Asociacion entre consumo de carne y casos activos de covid\n"
          "  [2] Paises con mayor cantidad de infectados con covid\n"
          "  [3] Obesidad y casos mortales vs saludable y personas recuperadas\n"
          "  [4] Consumo de alcohol y obesidad\n")

def pregunta_1(data):
#Asociacion entre cantidad de consumo de carne y casos de covid activos
    datos_1 = data.sort_values(by = ["Meat"],ascending = [False])
    datos_2 = data.sort_values(by = ["Meat"],ascending = [True])
    print("Asociacion entre consumo de carne y casos activos de covid\n\n")
    print(datos_1.iloc[[0,1,2,3,4],[0,9,29]],"\n")
    print(datos_2.iloc[[4,3,2,1,0],[0,9,29]],"\n")

def pregunta_2(data):
#Paises con mayor porcentaje de poblacion contagiada//QUIERO AGREGARLE UNA IMPRESION CON TEXTO PAIS-CASOS DECRECIENTE.
    data = data.sort_values(by = ["Active"],ascending = [False])
    print("Paises con mayor porcentaje de poblacion contagiada\n\n")
    print(data.iloc[[0,1,2,3,4],[0,29]],"\n")

def pregunta_3(data):
#Obesidad y los casos mortales de covid y los paises mas sanos con relacion a la cantidad de casos recuperados
    datos_1 = data.sort_values(by = ["Obesity"], ascending = [False])
    datos_1_temp = datos_1.iloc[[0,1,2,3,4],[0,24,27]]
    datos_2 = data.sort_values(by = ["Obesity"], ascending = [True])
    datos_2_temp = datos_2.iloc[[0,1,2,3,4],[0,24,28]]
    print("Obesidad y casos mortales de covid vs saludable y personas recuperadas\n")
    print(datos_1_temp)
    print(datos_2_temp,"\n")

def pregunta_4(data):
#Asociacion de consumo de alcohol y porcentaje de obesidad
    data = data.sort_values(by = ["Alcoholic Beverages"], ascending = [False])
    data = data.iloc[[0,1,2,3,4],[0,1,24]]
    print("Asociacion entre consumo de alcohol y la obesidad\n")
    print(data,"\n")

def selecciona_pregunta(opcion,data):

    while opcion != 1 and opcion != 2 and opcion != 3 and opcion != 4:
        clear()
        print("Opcion invalida, por favor asegurece de que su respuesta sea 1, 2, 3 o 4")
        menu()
        opcion = int(input("Opcion: "))
        clear()

    if opcion == 1:
        pregunta_1(data)
    elif opcion == 2:
        pregunta_2(data)
    elif opcion == 3:
        pregunta_3(data)
    else:
        pregunta_4(data)

def main():
    clear()
    data = abre_archivo()
    menu()
    opcion = int(input("Opcion: "))
    clear()
    selecciona_pregunta(opcion,data)

resp = "si"

while resp != "no":
    main()
    resp = input('¿Desea continuar?\n\nSi desea continuar escriba "si"\nSi no desea continuar escriba "no"\n\nRespuesta: ')
    clear()
    while resp != "si" and resp != "no":
        resp = input("Opcion invalida, por favor asegurece de que su respuesta sea 'si' o 'no'\n\nRespuesta: ")
        clear()
